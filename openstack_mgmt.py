# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
import argparse
#import urllib3
import json
import os
import time
import traceback
import subprocess
import sys


def _run(cmd, return_output=False):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate()
    output = stdout + stderr
    retcode = p.returncode
    #remove new line from last line
    output = output.rstrip()
    sys.stdout.flush()
    sys.stderr.flush()
    if (return_output == False):
        return retcode
    else:
        return retcode, output

class Mgmt():

    def __init__(self):
        auth_url = os.getenv("OS_AUTH_URL")
        username = os.getenv("OS_USERNAME")
        password = os.getenv("OS_PASSWORD")
        project_name = os.getenv("OS_PROJECT_NAME")
        user_domain_name = os.getenv("OS_USER_DOMAIN_NAME")
        project_domain_name = os.getenv("OS_PROJECT_DOMAIN_NAME")


        if not auth_url:
            print "FAIL: OS_AUTH_URL env is required"
            sys.exit(1)

    def _query(self, cmd):
        """
        Query openstack instance
        """
        try:
            #return self.nova.servers.list()
            rc, output = _run("openstack %s -f json" % cmd, return_output=True)
            if rc != 0:
                print ("FAIL: %s" % output)
                return None

            return json.loads(output)
        except:
            traceback.print_exc()
            print "FAIL: Could not get servers"
            return None


    def servers_get_all(self):
        return self._query("server list")

    def servers_list(self):
        servers = self.servers_get_all()
        keys = ("Name", "ID", "Status", "Networks")
        str_fmt = "%-30s %-40s %-10s %s"
        print (str_fmt % keys)
        if not servers:
            return None
        for server in servers:
            msg = str_fmt % (server["Name"], server["ID"],
                server["Status"], server["Networks"])
            print msg

    def get_server(self, name):
        servers = self.servers_get_all()
        if not servers:
            return None
        for server in servers:
            if (server["Name"] == name) or (server["ID"] == name):
                return server
        return None

    def server_get_id(self, name):
        server = self.get_server(name)
        if not server:
            return None
        return server["ID"]

    def get_server_status(self, name):
        server = self.get_server(name)
        if not server:
            return None
        return server["Status"]

    def server_add_floating_ip(self, name, ip):
        rc, output = _run("openstack server add floating ip %s %s" % (name, ip), return_output=True)
        if rc != 0:
            print ("FAIL: %s" % output)
            return False
        return True

    def server_get_floating_ip(self, name):
        cmd = "openstack server show %s -f value -c addresses | tr ',' '\n'|tr -d ' '| grep -v '='" % name
        rc, output = _run(cmd, return_output=True)
        if rc != 0:
            print ("FAIL: %s" % output)
            return None
        return output


    def server_create(self, name, image, flavor, nics=[], floating_ip_net=None,
                      wait=True, keyname=None, network=None):
        """
        nics:               List of network names to be assigned to instance
        floating_ip_net:    Network used to configure floating ip. Eg. 10.8.240.0
        wait:               If openstack command wait until server is up
        """
        #image_id = self.image_get_id(image)
        #if not image_id:
        #    print "FAIL: Could not find image: %s" % image
        #    return None
        #flavor_id = self.flavor_get_id(flavor)
        #if not flavor_id:
        #    print "FAIL: Could not find flavor: %s" % flavor
        #    return None

        if self.get_server(name):
            print("FAIL: Server %s already exist" % name)
            return None

        nova_nics = []
        if nics:
            networks = self.networks_get_all()
            if not networks:
                print("FAIL: Nics is set, but there is no network configured")
                return None
            for nic in nics:
                for net in networks:
                    if 'name' in net and  net['name'] == nic:
                        nova_nics.append({'net-id': net['id']})


        cmd = "server create"
        if wait:
            cmd += " --wait"
        if keyname:
            cmd += " --key-name %s" % keyname
        if network:
            cmd += " --network %s" % network
        cmd += " --flavor %s --image %s %s" % (flavor, image, name)
        print("INFO: Going to create server %s (%s)" % (name, cmd))
        server = self._query(cmd)
        if not server:
            self.server_delete(name)
            return None

        print("INFO: Waiting server %s to be ACTIVE" % name)
        timeout=5*60
        status = self.get_server_status(name)
        while  status == "BUILD" and timeout > 0:
            status = self.get_server_status(name)
            print ("INFO: Waiting server %s (%s) to be ACTIVE" % (name, status))
            timeout -= 1
            time.sleep(1)

        if timeout <= 0 or status == "ERROR":
            print "FAIL: Server has status %s and not ACTIVE" % status
            self.server_delete(name)
            return None

        msg = "INFO: Created server %s" % name

        if floating_ip_net:
            print("INFO: Trying add IP to server...")
            floating_ip = self.floating_ip_create(floating_ip_net)
            if not floating_ip:
                print("FAIL: Could not create floating ip")
                self.server_delete(name)
                return None
            print ("INFO: Allocated IP %s" % floating_ip)
            if not self.server_add_floating_ip(name, floating_ip):
                print "FAIL: Could not add floating ip to server. Delete floating ip and server..."
                traceback.print_exc()
                self.server_delete(name)
                self.floating_ip_delete(floating_ip)
                return None
            msg += " with IP: %s" % floating_ip
        print (msg)
        return name

    def server_delete(self, name):
        server_id = self.server_get_id(name)
        if not server_id:
            print("Fail: Could not find server %s" % name)
            return False
        floating_ip = self.server_get_floating_ip(name)
        rc, output = _run("openstack server delete --wait %s" % (name), return_output=True)
        if rc != 0:
            print("FAIL: %s" % output)
            return False
        print("INFO: Deleted server %s" % name)
        if floating_ip:
            self.floating_ip_delete(floating_ip)
            print("INFO: Released floating ip (%s)" % floating_ip)
        return True

    def flavors_get_all(self):
        return self._query("flavor list")

    def flavors_list(self):
        flavors = self.flavors_get_all()
        print("%-50s %s" % ("Name", "ID"))
        if not flavors:
            return None
        for flavor in flavors:
            print "%-50s %s" % (flavor["Name"], flavor["ID"])

    def flavor_get_id(self, name):
        flavors = self.flavors_get_all()
        if not flavors:
            return None
        for flavor in flavors:
            if (flavor["Name"] == name) or (flavor["ID"] == name):
                return flavor["ID"]
        return None

    def images_get_all(self):
        return self._query("image list")

    def images_list(self):
        images = self.images_get_all()
        if not images:
            return None
        for image in images:
            print("%s %s" % (image["Name"], image["ID"]))

    def image_get_id(self, name):
        images = self.images_get_all()
        if not images:
            return None
        for image in images:
            if (image["Name"] == name) or (image["ID"] == name):
                return image["ID"]
        return None


    def networks_get_all(self):
        return self._query("network list")

    def networks_list(self):
        networks = self.networks_get_all()
        keys = ("Name", "ID", "Subnets")
        str_fmt = "%-30s %-40s %s"
        print(str_fmt % keys)
        if not networks:
            return None
        for network in networks:
            print str_fmt % (network["Name"], network["ID"], network["Subnets"])

    def network_get_id(self, name):
        networks = self.networks_get_all()
        if not networks:
            return None
        for network in networks:
            if (network["Name"] == name) or (network["ID"] == name):
                return network["ID"]
        return None

    def network_delete(self, name):
        net_id = self.network_get_id(name)
        if not net_id:
            print "Fail: Could not find network %s" % name
            return True
        rc, output = _run("openstack network delete %s" % name, return_output=True)
        if rc != 0:
            print("FAIL: %s" % output)
            return False
        print("INFO: Deleted network %s" % (name))
        return True

    def floating_ips_get_all(self):
        return self._query("floating ip list")

    def floating_ips_list(self):
        ips = self.floating_ips_get_all()
        keys = ("Floating IP Address", "Fixed IP Address", "ID", "Floating Network")
        str_fmt = "%-20s %-20s %-40s %s"
        print(str_fmt % keys)
        if not ips:
            return None
        for ip in ips:
            print (str_fmt % (ip[keys[0]], ip[keys[1]], ip[keys[2]], ip[keys[3]]))

    def floating_ip_get_id(self, ip):
        floating_ips = self.floating_ips_get_all()
        if not floating_ips:
            return None
        for float_ip in floating_ips:
            if "Floating IP Address" in float_ip and float_ip["Floating IP Address"] == ip:
                return float_ip["ID"]
        return None

    def floating_ip_create(self, network_name):
        floating_ip = self._query("floating ip create %s" % network_name)
        if not floating_ip:
            print("FAIL: Could not create floating IP")
            return None
        if "floating_ip_address" not in floating_ip:
            print("FAIL: it seems wrong format of floating ip")
            print floating_ip
            return None
        return floating_ip["floating_ip_address"]

    def floating_ip_delete(self, ip):
        rc, output = _run("openstack floating ip delete %s" % ip, return_output=True)
        if rc != 0:
            print("FAIL: %s" % output)
            return False
        return True

    def keypair_create(self, name, private_key=None, public_key=None):
        if not private_key and not public_key:
            print("FAIL: keypair_create requires path to save new private key or path to publickey")
            return False
        cmd = None
        if private_key:
            cmd = "keypair create --private-key %s %s" % (private_key, name)
        if public_key:
            cmd = "keypair create --public-key %s %s" % (public_key, name)
        if self._query(cmd):
            return True
        return False

    def keypair_get_all(self):
        return self._query("keypair list")

    def keypair_list(self):
        keypairs = self.keypair_get_all()
        keys = ("Name",  "Fingerprint")
        str_fmt = "%-20s %s"
        print(str_fmt % keys)
        if not keypairs:
            return None
        for keypair in keypairs:
            print(str_fmt % (keypair[keys[0]], keypair[keys[1]]))
        return None

    def keypair_delete(self, name):
        rc, output = _run("openstack keypair delete %s" % name, return_output=True)
        if rc != 0:
            print("FAIL: %s" % output)
            return False
        return True

cmds = (
    dict(
        name='create_server',
        help="Create new server",
        args=[
            dict(name="--name -n", required=True, dest="s_name"),
            dict(name="--image -i", required=True, dest="s_image"),
            dict(name="--flavor -f", required=True, dest="s_flavor"),
            dict(name="--floating-net", required=False, dest="s_network"),
            dict(name="--key-name", required=False, dest="key_name"),
        ],
    ),
    dict(
        name='delete_server',
        help="Delete server",
        args=[
            dict(name="--name -n", required=True, dest="s_name"),
        ],
    )
)
def main():
    parser = argparse.ArgumentParser(description='Openstack manager')
    subparsers = parser.add_subparsers(help="Valid commands", dest="command")
    for cmd in cmds:
        _help = None
        if "help" in cmd:
            _help = cmd["help"]
        sub_parser = subparsers.add_parser(
            cmd['name'], help = _help )
        req_group = sub_parser.add_argument_group("required arguments")
        if "args" in cmd:
            for arg in cmd.get('args', []):
                name = arg.pop("name")
                required = False
                if "required" in arg:
                    required = arg.pop("required")
                if "dest" not in arg:
                    print("FAIL: dest needs to be set for arg: %s for command: %s" % (name, cmd["name"]))
                    sys.exit(1)
                name_list = name.split(" ")
                if required:
                    req_group.add_argument(*name_list, required=required, **arg)
                else:
                    sub_parser.add_argument(*name_list, required=required, **arg)

    obj = Mgmt()
    args = parser.parse_args()

    if not args.command:
        return obj

    if args.command == "create_server":
        obj.server_create(args.s_name, args.s_image, args.s_flavor, floating_ip_net=args.s_network,
                         keyname=args.key_name,)
        sys.exit(0)

    if args.command == "delete_server":
        obj.server_delete(args.s_name)
        sys.exit(0)

    print("FAIL: Unsupported command: %s" % args.command)
    sys.exit(1)


if __name__ == "__main__":
    main()

