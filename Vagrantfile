# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "centos/7"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"
   config.vm.network "private_network", ip: "192.168.121.229"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  config.vm.provider "libvirt" do |libvirt|
        libvirt.memory = 8192
        libvirt.cpu_mode = "host-model"
  end
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL
    export LANG=en_US.utf-8
    export LC_ALL=en_US.utf-8

    systemctl stop NetworkManager
    systemctl disable NetworkManager
    systemctl mask NetworkManager
    yum remove NetworkManager -y

    #Workaround due https://github.com/nofdev/fastforward/issues/5
    mkdir -p /var/cache/swift
    chmod -R 775 /var/cache/swift

    yum install -y wget

    yum install -y centos-release-openstack-pike
    yum install -y openstack-packstack
    packstack --allinone --provision-demo=n --os-neutron-ovs-bridge-mappings=extnet:br-ex --os-neutron-ovs-bridge-interfaces=br-ex:eth0 --os-neutron-ml2-type-drivers=vxlan,flat --keystone-admin-passwd=admintest
    source /root/keystonerc_admin

    wget --quiet https://download.fedoraproject.org/pub/fedora/linux/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2 -O /tmp/Fedora-Cloud-Base-27-1.6.x86_64.qcow2
    yum install -y libguestfs-tools-c
    virt-customize  --root-password password:fedora -a /tmp/Fedora-Cloud-Base-27-1.6.x86_64.qcow2
    openstack image create --disk-format qcow2 --file /tmp/Fedora-Cloud-Base-27-1.6.x86_64.qcow2 Fedora-27
    openstack image add project Fedora-27 admin
    rm -f /tmp/Fedora-Cloud-Base-27-1.6.x86_64.qcow2

    openstack network create internal
    openstack subnet create --network internal --allocation-pool start=172.168.121.115,end=172.168.121.125 --gateway 172.168.121.1 --subnet-range 172.168.121.0/24 internal

    openstack network create --external --default --provider-physical-network=extnet --provider-network-type=flat external
    openstack subnet create --network external --allocation-pool start=192.168.121.115,end=192.168.121.125 --gateway 192.168.121.1 --subnet-range 192.168.121.0/24 external

    openstack router create route1
    openstack router add subnet route1 internal
    openstack router set --external-gateway external route1

    #iptables can block floating ips
    iptables -F
    service iptables save

    #show credentials
    cat /root/keystonerc_admin
   SHELL
end
